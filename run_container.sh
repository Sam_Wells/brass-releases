#!/bin/bash

set -a
source vars.sh > /dev/null 2>&1 || { echo "Missing configuration, please run the configure.sh script first!" ; exit 1; }

if [[ $CONFIG_COMPLETE != 1 ]]; then
    echo "Invalid configuration, please re-run the configure.sh script."
    exit 1
fi

usage() { echo "Usage: $0 [-b|--build-only] [-r|--run-only <path>] [-t|--test <test name>] [ARCH=<arch>] [CROSS_COMPILE=<path>]" 1>&2; exit 1; }

# Parse Arguments
CC_STR=""
BUILD=true
RUN=true
KMOD_PATH="./soteria.ko"
TEST=""

while [ -n "$1" ] ; do
    case "$1" in
        "--build-only"|"-b")
            RUN=false
            shift
            ;;
        "--run-only"|"-r")
            if [[ "$2" == "" ]]; then
                echo "Missing driver path.  Please specify the path to the soteria driver."
                usage
            fi
            BUILD=false
            KMOD_PATH=$2
            shift
            shift
            ;;
        "--test"|"-t")
            TEST="--test $2"
            shift
            shift
            ;;
        ARCH=*)
            CC_STR="${CC_STR} ARCH=${1#*=} "
            shift
            ;;
        CROSS_COMPILE=*)
            CC_STR="${CC_STR} CROSS_COMPILE=${1#*=} "
            shift
            ;;
        *)
            echo "Unknown argument $1"
            usage
            ;;
    esac
done;

if [ "$BUILD" = true ]; then
    cd driver/
    make KBUILD=$KERN_BUILD_PATH clean
    make SYSTEM_MAP=$SYSTEM_MAP_FILE_PATH KBUILD=$KERN_BUILD_PATH $CC_STR || exit 1
    if [[ $SIGNING_REQUIRED == 1 ]]; then
        $SIGN_FILE_UTIL_PATH $SIGN_FILE_UTIL_HASH_ALGO $SIGN_FILE_UTIL_PRIV_KEY $SIGN_FILE_UTIL_PUB_KEY ./soteria.ko
    fi
    cp -f ./soteria.ko ../soteria.ko
    cd ..


    export KBUILD=$KERN_BUILD_PATH
    make -C ./userspace/mod/ clean
    make -C ./userspace/mod/

    if [[ $SIGNING_REQUIRED == 1 ]]; then
        $SIGN_FILE_UTIL_PATH $SIGN_FILE_UTIL_HASH_ALGO $SIGN_FILE_UTIL_MODSIGN_PRIV_KEY $SIGN_FILE_UTIL_MODSIGN_PUB_KEY ./userspace/mod/mod.ko
    fi

    make -C ./userspace/suid/

fi

if [ "$RUN" = true ]; then
    echo "**************************************** WARNING!!! *******************************************"
    echo ""
    echo "This test will load a vulnerable driver and perform security tests within the running kernel!"
    echo "Running these tests will temporarily introduce vulnerabilities into the kernel and may leave"
    echo "your system in an inconsistent state.  Please ensure you are not about to run this test on"
    echo "any device that is mission critical!"
    echo ""
    echo "In order to ensure your system returns to a consistent state, we recommend rebooting the target"
    echo "machine after running the tests!  Nothing in these tests will persist across a reboot."
    echo ""
    echo "Additionally, this script uses the 'sudo' command and will require administrator privileges."
    echo ""
    echo "BedRock Systems and the authors of this script cannot be held liable for any damages, use at"
    echo "your own risk!"
    echo ""
    echo -n "If you have read and agree with the above, press 'Y' to continue: "

    read -r -n 1 -s answer

    echo ""

    if [[ $answer != [Yy] ]]; then
        exit 0
    fi


    PANIC_ON_OOPS=$(sysctl -n kernel.panic_on_oops)
    sysctl kernel.panic_on_oops=0
    rmmod soteria > /dev/null 2>&1
    rm -rf /dev/bedrock > /dev/null 2>&1
    insmod $KMOD_PATH
    MAJOR=$(dmesg | tail -100 | grep major | tail -1 | awk -F\= '{print $2}'| awk '{print $1}')
    MINOR=$(dmesg | tail -100 | grep major | tail -1 | awk -F\= '{print $3}'| awk '{print $1}')
    mknod /dev/bedrock c $MAJOR $MINOR
    cd userspace/
    echo ""
    echo "--------------------------------------------------------------"
    # taskset -a 0x1 ./run.py
    ./run.py $TEST
    echo "--------------------------------------------------------------"
    cd ../
    rm -rf /dev/bedrock
    rmmod soteria
    sysctl kernel.panic_on_oops=$PANIC_ON_OOPS
fi
